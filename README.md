### [ReScience C](https://rescience.github.io/) article template

This repository provides the source for a check on reproducing results from
at least one section of a paper published in 2008. The attempt to reproduce
the results constitutes the reproducibility research article
```
Roukema, Boudewijn F., 2020, ReScience C, 6, #11, 1
```
* Version of record: https://zenodo.org/record/3956058
* ArXiv: https://arxiv.org/abs/2008.07380
* Open peer review: https://github.com/ReScience/submissions/issues/41
* Journal interface: https://rescience.github.io/read/#volume-6-2020
* Article source: https://codeberg.org/boud/RBG08
* Code: https://codeberg.org/boud/0807.4260
* SwH: https://archive.softwareheritage.org/swh:1:dir:4f1fe8cf5a01bb4637e31ea938eaa5bc25a2b87b
* Data: https://lambda.gsfc.nasa.gov/data/map/dr3/dfp/wmap_ilc_5yr_v3.fits

The generic instructions below will typically apply to any new submissions to ReScience C.

#### Usage

For a submission, fill in information in
[metadata.yaml](./metadata.yaml), modify [content.tex](content.tex)
and type:

```bash
$ make
```

This will produce an `article.pdf` using xelatex and provided font. Note that you must have Python 3 and [PyYAML](https://pyyaml.org/) installed on your computer, in addition to `make`.


After acceptance, you'll need to complete [metadata.yaml](./metadata.yaml) with information provided by the editor and type again:

```bash
$ make
```

(C) 2015-2020, Nicolas Rougier + co-authors GPL-3+, Apache v2+, SIL Open Font License

This set of template files is free-licensed. The files contained in
the sub-directories roboto/; source-code-pro/; source-sans-pro/;
source-serif-pro/; have their free licences indicated with a
"*License.txt" file. All other files, including this one, are licensed
under the GPL version 3 or later, at your choosing, by Nicolas Rougier
and co-authors, 2015-2020. See the file COPYING for details of the
GPL-3 licence.
